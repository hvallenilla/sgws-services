<?php

/**
 * home actions.
 *
 * @package    lynx
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    if(!$this->getUser()->hasCredential('backend_activo')){
      $this->redirect('@homepage');
    }
    $this->notis =  NotificacionesDestinatariosPeer::getNewsNotificacionUser($this->getUser()->getAttribute('idUserPanel'));
    $this->lastUpdateTimeSheet = TempotarefaPeer::getLastUpdateTimeSheet(aplication_system::getUser());
    
    $this->meusProjetos = PropostaPeer::getNumMeusProjetos(aplication_system::getUser()) + EquipeTarefaPeer::getTotalProjetosDeFuncionario(aplication_system::getUser());
    $tentradas = SaidasPeer::getTotalByOperacionUsuario(aplication_system::getUser(), 'e' , true);
    $tsalidas = SaidasPeer::getTotalByOperacionUsuario(aplication_system::getUser(), 's', false);
    //$this->minhasDespesas = SaidasPeer::getNumMeusProjetos(aplication_system::getUser());
    $this->minhasDespesas = $tentradas - $tsalidas;
    
    
    
  }
}
