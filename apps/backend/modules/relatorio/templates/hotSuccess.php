<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
    <thead>
        <tr>
            <th style="width: 10%; padding-left: 10px;">Data Emissão</th>
            <th class="">Proposta</th>
            <th class="" style="">Cliente</th>
            <th class="">Gerente</th>
            <th class="">Tipo de Serviço</th>
            <th class="">Valor</th>
        </tr>
    </thead>
    <tbody>
        <?php if($result): ?>
            <?php foreach ($result as $dato) : ?>
                <tr>
                    <td><?php echo date("d-m-Y", strtotime($dato['data'])); ?></td>
                    <td><?php echo $dato['proposta'] ?></td>
                    <td><?php echo $dato['cliente'] ?></td>
                    <td><?php echo $dato['gerente'] ?></td>
                    <td><?php echo $dato['tipo'] ?></td>
                    <td><?php echo $dato['valor'] ?></td>
                    
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
                <tr>
                    <td colspan="6" class="center erro_no_data">Nenhum resultado</td>
                </tr>
        <?php endif; ?>
    </tbody>
</table>