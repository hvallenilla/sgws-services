<?php use_stylesheet('tableRelatorio.css') ?>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
    <thead>
        <tr>
            <th style="width: 10%; padding-left: 4px;">Data</th>
            <th class="">Projeto</th>
            <th class="">Cliente</th>
            <th class="">Gerente</th>
            <th class="">Tipo de Serviço</th>
            <th class="">Valor R$</th>
        </tr>
    </thead>
    <tbody>
        <?php if($result): ?>
            <?php foreach ($result as $dato) : ?>
                <tr>
                    <td><?php echo $dato['data'] ?></td>
                    <td><?php echo $dato['projeto'] ?></td>
                    <td><?php echo $dato['cliente'] ?></td>
                    <td><?php echo $dato['gerente'] ?></td>
                    <td><?php echo $dato['tipo'] ?></td>
                    <td><?php echo $dato['valor'] ? 'R$ '.aplication_system::monedaFormat($dato['valor']) : '' ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>