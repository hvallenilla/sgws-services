<?php use_stylesheet('tableRelatorio.css') ?>

<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
    <thead>
        <tr>
            <th style="padding-left: 10px;">Projeto</th>
            <th class="">Cliente</th>
            <th class="">Gerente</th>
            <th class="">Valor</th>
            <th class="">Data Vencimento</th>
            <th class="">Dias de Atraso</th>
        </tr>
    </thead>
    <tbody>
        <?php if($result): ?>
            <?php foreach ($result as $v) : ?>
                <tr>
                    <td style="padding-left: 10px;"><?php echo $v['projeto'] ?></td>
                    <td style="width: 30%;"><?php echo $v['cliente'] ?></td>
                    <td><?php echo $v['gerente'] ?></td>
                    <td>R$ <?php echo aplication_system::monedaFormat($v['valor']) ?></td>
                    <td><?php echo $v['fecha'] ?></td>
                    <td><?php echo $v['dias_atraso'] ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>