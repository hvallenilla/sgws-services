<?php use_stylesheet('tableRelatorio.css') ?>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList" >
    <thead>
        <tr>
            <th style="padding-left: 10px; width: 10%;">Data</th>
            <th class="" style="width: 6%">Proposta</th>
            <th class="" style="width: 20%">Cliente</th>
            <th class="" style="width: 20%">Gerente</th>
            <th class="">Tipo de Serviço</th>
            <th class="">Valor</th>
        </tr>
    </thead>
    <tbody>
        <?php if($result): ?>
            <?php foreach ($result as $dato) : ?>
                <tr>
                    
                    <td style="padding-left: 10px;" ><?php echo $dato['data'] ?></td>
                    <td style="padding-left: 10px;" ><?php echo $dato['proposta'] ?></td>
                    <td><?php echo $dato['cliente'] ?></td>
                    <td><?php echo $dato['gerente'] ?></td>
                    <td><?php echo $dato['tipo'] ?></td>
                    <td>R$ <?php echo $dato['valor'] ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>