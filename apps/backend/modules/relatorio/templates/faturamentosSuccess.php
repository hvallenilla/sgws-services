<?php use_stylesheet('tableRelatorio.css') ?>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
    <thead>
        <tr>
            <th style="padding-left: 10px;">Faturamento</th>
            <?php for($i =  1; $i <=12; $i++): ?>
            <?php $nMes = globalFunctions::zerofill($i,2) ?>
            <th class="center"><?php echo lynxValida::nombreMes($nMes) ?></th>
            <?php endfor; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $key => $value): ?>
        <tr>
            <td><?php echo $key ?></td>            
            <?php foreach ($value as $k => $v): ?>
                <td>R$ <?php echo $v ?></td>            
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
    
</table>