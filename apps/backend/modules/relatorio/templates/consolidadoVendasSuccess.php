<?php use_stylesheet('tableRelatorio.css') ?>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
    <thead>
        <tr>
            <th style="padding-left: 10px;">Mês</th>
            <th class="">Propostas Emitidas</th>
            <th class="">Total Emitido ($$)</th>
            <th class="">Valor médio Emitido</th>
            <th class="">Propostas Vendidas</th>
            <th class="">Total Vendido ($$)</th>
            <th class="">Valor Médio Vendido</th>
        </tr>
    </thead>
    <tbody>
        <?php if($result): ?>
            <?php foreach ($result as $mes => $calculos) : ?>
                <tr>
                    <td><?php echo $mes ?></td>
                    <?php foreach ($calculos as $value) : ?>
                    <td><?php echo $value ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>