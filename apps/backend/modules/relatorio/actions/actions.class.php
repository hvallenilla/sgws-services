<?php

/**
 * relatorio actions.
 *
 * @package    sgws
 * @subpackage relatorio
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class relatorioActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->ano = $request->getParameter('ano') ? $request->getParameter('ano') : date('Y');
    $range  = range(date('Y') - 1, date('Y'));
    $this->years = array_combine($range,$range);
  }
  
  public function executeWip(sfWebRequest $request)
  {
    $this->status = StatusPeer::getListStatusFilter();
    if (!$request->isMethod('post'))
    {
        $this->statusSelected = 4; // En andamento por defecto
    }else{
        $this->statusSelected = $request->getParameter('proposta_status');
    }
    $this->result = PropostaPeer::getAllProjetos($this->statusSelected, $request->getParameter('buscador'));
  }
  
  public function executeConsolidadoProjeto(sfWebRequest $request)
  {
      if (!$request->isMethod('post'))
        {
            $this->statusSelected = 4; // En andamento por defecto
            $request->setAttribute('proposta_status', 4);
        }else{
            $this->statusSelected = $request->getParameter('proposta_status');
            $request->setAttribute('proposta_status', 4);
        }
      $this->result = PropostaPeer::getConsolidadoProjetos($this->statusSelected, $request->getParameter('buscador'));
      $this->status = StatusPeer::getListStatusFilter();
  }
  
  public function executeFluxo(sfWebRequest $request)
  {
      $this->data = PropostaPeer::retrieveByPK($request['id']);
      $this->despesa = SaidasPeer::getSaidaPerProjeto($request['id']);
  }
  
  public function executeFinanciero(sfWebRequest $request)
  {
      
  }
  
  public function executeBilability(sfWebRequest $request)
  {
      $this->setLayout('layoutTotalAncho');
      $ano = 2013;
      $funcionarios = LxUserPeer::getFuncionariosByBilability();
      $horasBilability = HorasBillabilityPeer::getAnoHorasMes($ano);
      $mesActual = date('m');
      
        foreach ($funcionarios as $funcionario) 
        {
            
            // Recorro por meses y analizo de acuerdo al status
            $totalHorasBilability = 0;
            $HorasTrabajadasMedia = 0;
            $media = 0;
            for($i =  1; $i <=12; $i++)
            {
                $nMes = globalFunctions::zerofill($i,2);
                $nombreMes = lynxValida::nombreMes($nMes);
                
                $valor = TempotarefaPeer::getHorasTrabajadasFuncionario($ano,$nMes,$funcionario['id'], 1); 
                $getMes = 'getMes'.$i;
                // Horas bilability del mes
                $horasBilabilityMes = $horasBilability->$getMes();
                // Total horas bilability
                if($i <= $mesActual)
                {
                    $totalHorasBilability = $totalHorasBilability + $horasBilabilityMes;
                    $HorasTrabajadasMedia = $HorasTrabajadasMedia + $valor;
                }
                
                // porcentaje de las horas trabajadas del funcionario en el mes
                $porcentajeMesFuncionario = $valor * 100 / $horasBilabilityMes;
                //$r[$funcionario['nome']][$funcionario['cargo']][$nombreMes.' '.$horasBilability->$getMes()] = $valor;
                $r[$funcionario['nome']][$funcionario['cargo']][$nMes] = array($valor,$porcentajeMesFuncionario);
                if($i == 12)
                {
                    $this->media = $totalHorasBilability / $mesActual;
                    $this->mediaHoras = $HorasTrabajadasMedia / $mesActual;
                    
                    $r[$funcionario['nome']][$funcionario['cargo']]['Media'] = array($HorasTrabajadasMedia,$this->mediaHoras);
                    $r[$funcionario['nome']][$funcionario['cargo']]['Meta'] = $funcionario['meta'];
                    $r[$funcionario['nome']][$funcionario['cargo']]['M. Atingida?'] = 0;
                }
            }  
          
            
          
        }
        $this->result = $r;
//      echo "<pre>";
//      print_r($r);
//      echo "</pre>";
//      die();
  }
  
  public function executeNbilability(sfWebRequest $request)
  {
      $this->setLayout('layoutTotalAncho');
      $ano = 2013;
      $funcionarios = LxUserPeer::getFuncionariosByBilability();
      $horasBilability = HorasBillabilityPeer::getAnoHorasMes($ano);
      $mesActual = date('m');
      
        foreach ($funcionarios as $funcionario) 
        {
            
            // Recorro por meses y analizo de acuerdo al status
            $totalHorasBilability = 0;
            $HorasTrabajadasMedia = 0;
            $media = 0;
            for($i =  1; $i <=12; $i++)
            {
                $nMes = globalFunctions::zerofill($i,2);
                $nombreMes = lynxValida::nombreMes($nMes);
                
                $valor = TempotarefaPeer::getHorasTrabajadasFuncionario($ano,$nMes,$funcionario['id'], 2); 
                $getMes = 'getMes'.$i;
                // Horas bilability del mes
                $horasBilabilityMes = $horasBilability->$getMes();
                // Total horas bilability
                if($i <= $mesActual)
                {
                    $totalHorasBilability = $totalHorasBilability + $horasBilabilityMes;
                    $HorasTrabajadasMedia = $HorasTrabajadasMedia + $valor;
                }
                
                // porcentaje de las horas trabajadas del funcionario en el mes
                $porcentajeMesFuncionario = $valor * 100 / $horasBilabilityMes;
                //$r[$funcionario['nome']][$funcionario['cargo']][$nombreMes.' '.$horasBilability->$getMes()] = $valor;
                $r[$funcionario['nome']][$funcionario['cargo']][$nMes] = array($valor,$porcentajeMesFuncionario);
                if($i == 12)
                {
                    $this->media = $totalHorasBilability / $mesActual;
                    $this->mediaHoras = $HorasTrabajadasMedia / $mesActual;
                    
                    $r[$funcionario['nome']][$funcionario['cargo']]['Media'] = array($HorasTrabajadasMedia,$this->mediaHoras);
                    $r[$funcionario['nome']][$funcionario['cargo']]['Meta'] = $funcionario['meta'];
                    $r[$funcionario['nome']][$funcionario['cargo']]['M. Atingida?'] = 0;
                }
            }  
          
            
          
        }
        $this->result = $r;
//      echo "<pre>";
//      print_r($r);
//      echo "</pre>";
//      die();
  }

  public function executeFaturamentos(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $t = array('1' => 'Faturamento Realizado', '2' => 'Previsão de Faturamento', '3' => 'Total');
      $r = array();
      $ano = 2013;
      foreach ($t as $status => $valorStatus) {
          // Recorro por meses y analizo de acuerdo al status
          //$total = 0;
          for($i =  1; $i <=12; $i++)
          {
              $nMes = globalFunctions::zerofill($i,2);
              
              if($status == 1)
              {
                  $valor = SaidasPeer::getFaturamentosRealizados($ano,$nMes);
              }elseif ($status == 2){
                  $valor = SaidasPeer::getFaturamentosPrevistos($ano,$nMes);
              }
              
              //echo $nMes.'--'.$valor."<br>";
              //$total = $total + $valor;
              // Grabo en array
              if($status < 3)
              {
                  $r[$valorStatus][$nMes] = aplication_system::monedaFormat($valor);
              }else{
                  //$r[$valorStatus][$nMes] = aplication_system::monedaFormat($total);
              }
              $total = 0;
          }
      }
//      print_r($r);
//      echo "</pre>";
//      die();
      $this->result = $r;
      
  }
  
  public function executeDespesas(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $this->titulo = 'Despesas';
      $this->variables = array('0' => 'Despesas Realizadas', '3' => 'Previsão de Despesas PJ', '4' => 'Previsão de Despesas ADM');
      $r = array();
      $this->totales = array();
      $ano = 2013;
      foreach ($this->variables as $status => $valorStatus) {
            // Recorro por meses y analizo de acuerdo al status
            for($i =  1; $i <=12; $i++)
            {
                $nMes = globalFunctions::zerofill($i,2);
                $valor = SaidasPeer::getDespesasRealizadosPerCentro($ano,$nMes,$status); 
                $r[$status][$nMes] = $valor;
            }   
      }
      for($i =  1; $i <=12; $i++)
      {
          $nMes = globalFunctions::zerofill($i,2);
          $tMes =  $r[0][$nMes] + $r[3][$nMes] + $r[4][$nMes];
          $this->totales[$nMes] = $tMes;
      }
      $this->result = $r;
//      echo "<pre>";
//      print_r($r);
//      print_r($this->totales);
//      echo "</pre>";
//      die();
  }
  
  public function executePagamentosEmAtraso(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $this->result = SaidasPeer::getPagamentosEmAtraso();
  }
  
  public function executeFluxoCaixa(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $this->setTemplate('despesas');
      $this->titulo = 'Fluxo de Caixa';
      $this->variables = array(
          '1' => 'Faturamentos', 
          '2' => 'Imposto', 
          '3' => 'Despesas PJ', 
          '4' => 'Despesas ADM',
          '5' => 'Despesas Comerciais',
          '6' => 'Despesas Terceiros',
          '7' => 'Despesas Comerciais',
          '8' => 'Despesas Treinamento'
      );
      $r = array();
      $this->totales = array();
      $ano = 2013;
      foreach ($this->variables as $status => $valorStatus) {
            // Recorro por meses y analizo de acuerdo al status
            for($i =  1; $i <=12; $i++)
            {
                $nMes = globalFunctions::zerofill($i,2);
                $valor = SaidasPeer::getDespesasRealizadosPerCentro($ano,$nMes,$status); 
                $r[$status][$nMes] = $valor;
            }   
      }
      for($i =  1; $i <=12; $i++)
      {
          $nMes = globalFunctions::zerofill($i,2);
          $tMes =  $r[0][$nMes] + $r[3][$nMes] + $r[4][$nMes];
          $this->totales[$nMes] = $tMes;
      }
      $this->result = $r;
  }
  
  public function executeVendas(sfWebRequest $request)
  {
      
  }
  
  public function executeConsolidadoVendas(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $r = array();
      $ano = 2013;
//      echo "<pre>";
      for($i =  1; $i <=12; $i++)
      {
            $nMes = globalFunctions::zerofill($i,2);
            $nombreMes = lynxValida::nombreMes($nMes).'<br>';
            // busco las propuestas emitidas
            $propostasEmitidas = PropostaPeer::getPropostasEmitidas($ano, $nMes);
            $propostasVendidas = PropostaPeer::getPropostasVendidas($ano, $nMes);
            $r[$nombreMes]= array(
                  'propostas_emitidas'     => $propostasEmitidas['cantidad'],
                  'total_emitido'         => 'R$ '.aplication_system::monedaFormat($propostasEmitidas['total']),
                  'valor_medio_emitido'    => $propostasEmitidas['valor_medio_emitido'],
                  'propostas_vendidas'    => $propostasVendidas['cantidad'],
                  'total_vendido'    => 'R$ '. aplication_system::monedaFormat($propostasVendidas['total']),
                  'valor_medio_vendido'    => $propostasVendidas['valor_medio_vendido']
              );
      }
      $this->result = $r;
//      print_r($r);
//      echo "</pre>";    
//      die();
  }
  
  public function executeFunilVendas(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $r = array();
      $data = array();
      $ano = 2013;
      $projetos = PropostaPeer::getFunilVendasProjetos($ano);

      foreach ($projetos as $pro) {

          $data['data'] = date("d-m-Y", strtotime($pro['data']));
          $data['projeto'] = $pro['projeto'];
          $data['cliente'] = $pro['cliente'];
          $data['gerente'] = $pro['gerente'];
          $data['tipo'] = $pro['tipo'];
          $data['valor'] = $pro['valor'];
          $datos[] = $data;
          
      }
      $this->result =  $datos;
      
  }
  
  public function executeEmNegociacao(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $r = array();
      $data = array();
      $ano = 2013;
      $propostas = PropostaPeer::getPropostasEnNegociacao($ano);

      foreach ($propostas as $pro) {

          $data['data'] = date("d-m-Y", strtotime($pro['data']));
          $data['proposta'] = $pro['proposta'];
          $data['cliente'] = $pro['cliente'];
          $data['gerente'] = $pro['gerente'];
          $data['tipo'] = $pro['tipo'];
          $data['valor'] = $pro['valor'];
          $datos[] = $data;
          
      }
      $this->result =  $datos;
  }
  
  public function executeHot(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $r = array();
      $data = array();
      $ano = 2013;
      $this->result = PropostaPeer::getPropostasHot();
      
      
  }
  
  
  
  
  
  
  
}
