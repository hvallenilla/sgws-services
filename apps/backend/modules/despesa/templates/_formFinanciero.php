<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript("jq/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-br.js") ?>
<script type="text/javascript"> 
    var url_fun = 'http://' + location.hostname + '/backend_dev.php';
    
    $(document).ready(function() {
        
        <?php if(!$form->getObject()->isNew() && $form->getObject()->getCodigoTipo()): ?>
            cargaTipos(<?php echo $form->getObject()->getCodigoTipo() ?>, $('#saidas_operacao').val());
            cargaSubTiposSimple(<?php echo $form->getObject()->getCodigoTipo() ?>, '<?php echo $form->getObject()->getCodigoSubtipo() ?>');
            cargaFornecedor('<?php echo $form->getObject()->getCodigocadastro() ?>','<?php echo $form->getObject()->getCodigoSubtipo() ?>');
            <?php if($form->getObject()->getOperacao() == 'e' ): ?>
                $("#lb-cadastro").html('Cliente');
            <?php else: ?>
                $("#lb-cadastro").html('Fornecedor');
            <?php endif; ?>
                
        <?php else: ?>
            cargaTipos('0', $('#saidas_operacao').val());
        <?php endif; ?>
        $('.data-despesa').each(function(){
            $(this).datepicker({   
                defaultDate: "+1w",
                dateFormat: 'dd-mm-yy',        
                changeMonth: true,
                changeYear: true
            }); 
        });
        
        $('#saidas_operacao').change(function(){
            
            if($(this).val() == 'e' )
                {
                    $("#lb-cadastro").html('Cliente');
                    $("#fecha-faturamento").show();
                    cargaAdministradores();
                }else{
                    $("#fecha-faturamento").hide();
                    $("#lb-cadastro").html('Fornecedor');
                    cargaFuncionarios('0');
                    clearSelectOptions('funcionario');
                }
            $("#saidas_codigo_tipo > option").remove();
            $("#subtipo > option").remove();
            $("#fornecedor > option").remove();
            $("#saidas_codigo_tipo").append("<option value=''>Carregando...</option>");
            $("#subtipo").append("<option value=''>Selecione...</option>");
            $("#fornecedor").append("<option value=''>Selecione...</option>");
            cargaTipos($("#saidas_codigo_tipo").val(), $('#saidas_operacao').val());
        });
        
        $('.tipos').change(function(){
            cargaSubTiposSimple($('#saidas_codigo_tipo').val(),'0');
        });
        $('#subtipo').change(function(){
             cargaFornecedor('0',$("#subtipo").val());
        });
        $('#saidas_centro').change(function()
        {
            $("#saidas_operacao").removeAttr("disabled");
            $('#saidas_codigo_tipo').removeAttr("disabled");
            $('#subtipo').removeAttr("disabled");
            $('#fornecedor').removeAttr("disabled");
            
            if($(this).val() == 'projeto')
            {
                $("#projeto-tr").show();
                $("#funcionario-tr").show();
                $('#saidas_operacao option[value="e"]').attr("selected", "selected");
                if($('#saidas_operacao').val() != 'e')
                    {
                        <?php if(!$form->getObject()->isNew() && $form->getObject()->getCodigofuncionario()): ?>
                            cargaFuncionariosProjeto($('#saidas_codigoprojeto').val(), <?php echo $form->getObject()->getCodigofuncionario() ?> );
                        <?php else: ?>
                            cargaFuncionariosProjeto($('#saidas_codigoprojeto').val());
                        <?php endif; ?>
                    }
                    if($('#saidas_operacao').val() == 'e')
                        {cargaAdministradores();}
            }else{
                //$("#projeto-tr").hide();
                $("#funcionario-tr").hide();
            }
            if($(this).val() == 'compensação')
            {
                $("#projeto-tr").hide();
                $('#saidas_codigo_tipo').attr("disabled", "disabled");
                $('#subtipo').attr("disabled", "disabled");
                $('#fornecedor').attr("disabled", "disabled");
                $('#saidas_operacao option[value="e"]').attr("selected", "selected");
                $('#saidas_operacao').attr("disabled", "disabled");
                $("#funcionario-tr").show();
                cargaFuncionarios('0');
                
            }
            if($(this).val() == 'adiantamento')
            {
                $("#l-dataemissao").hide();
                $("#c-dataemissao").hide();
            }else{
                $("#l-dataemissao").show();
                $("#c-dataemissao").show();
            }
            if($(this).val() == 'comercial' || $(this).val() == 'adiantamento')
            {
                $("#funcionario-tr").show();
                
                if($(this).val() == 'comercial')
                {
                    // El usuario logeado es el funcionario
                    cargaFuncionarios('<?php echo aplication_system::getUser() ?>');
                    //$("#funcionario > option").remove();
                    //$("#funcionario").append("<option value='<?php echo aplication_system::getUser() ?>'><?php echo $sf_user->getAttribute('nameUser') ?></option>");
                }
                if($(this).val() == 'adiantamento')
                {
                    
                    // Ocultar registro y marcar como entrada
                    $("#projeto-tr").hide();
                    cargaFuncionarios('0');
                    $('#saidas_operacao option[value="e"]').attr("selected", "selected");
                    $('#saidas_operacao').attr("disabled", "disabled");
                    $('#saidas_codigo_tipo').attr("disabled", "disabled");
                    $('#saidas_codigo_tipo option[value=""]').attr("selected", "selected");
                    $('#subtipo').attr("disabled", "disabled");
                    $('#subtipo option[value=""]').attr("selected", "selected");
                    $('#fornecedor').attr("disabled", "disabled");
                    $('#fornecedor option[value=""]').attr("selected", "selected");
                }
            }
        });
        <?php if(!$form->getObject()->isNew()): ?>
            <?php if($form->getObject()->getCentro() == 'adiantamento' ): ?>
                $('#projeto-tr').hide();
                $("#l-dataemissao").hide();
                $("#c-dataemissao").hide();
            <?php else: ?>
                $("#l-dataemissao").show();
                $("#c-dataemissao").show();
            <?php endif; ?>
            <?php if($form->getObject()->getCentro() == 'compensação'): ?>
                $('#projeto-tr').hide();
                $('#saidas_codigo_tipo').attr("disabled", "disabled");
                $('#subtipo').attr("disabled", "disabled");
                $('#fornecedor').attr("disabled", "disabled");
                $('#saidas_operacao option[value="e"]').attr("selected", "selected");
                $('#saidas_operacao').attr("disabled", "disabled");
                cargaFuncionarios('0');
            <?php endif; ?>
        <?php endif; ?>
        
        // Pendiente: http://bseth99.github.io/jquery-ui-extensions/tests/visual/combobox/base.html
        $('#saidas_codigoprojeto').change(function(){
            
            if($('#saidas_codigoprojeto').val() > 0)
            {
                $('#saidas_centro option[value="projeto"]').attr("selected", "selected");
                //$('#saidas_operacao option[value="e"]').attr("selected", "selected");
            }
            // cargaFuncionariosProjeto($('#saidas_codigoprojeto').val());
            
            cargaAdministradores();            
        });
        
        $("#despesa").submit(function(){
            $("#saidas_operacao").removeAttr("disabled");
            return true;
        })
        
        formatInputMoneda($('#saidas_saidaprevista'));
        formatInputMoneda($('#saidas_saidas'));
        if($('#saidas_operacao').val() == 'e' && $('#saidas_centro').val() != 'compensação' && $('#saidas_centro').val() != 'adiantamento')
        {cargaAdministradores();}
        
        <?php if(!$form->getObject()->isNew()): ?>
            
            $("#saidas_dataprevista").val('<?php echo $form->getObject()->getDataprevista() ? date("d-m-Y", strtotime($form->getObject()->getDataprevista())) : '' ?>');
            $("#saidas_datareal").val('<?php echo $form->getObject()->getDatareal() ? date("d-m-Y", strtotime($form->getObject()->getDatareal())) : '' ?>');
            $("#saidas_dataemissao").val('<?php echo $form->getObject()->getDataemissao() ? date("d-m-Y", strtotime($form->getObject()->getDataemissao())) : ''  ?>');
            $("#saidas_datarecebimentopre").val('<?php echo $form->getObject()->getDatarecebimentopre() ? date("d-m-Y", strtotime($form->getObject()->getDatarecebimentopre())) : ''  ?>');
            <?php if($form->getObject()->getOperacao() == 's' ): ?>
                $("#fecha-faturamento").hide();
            <?php endif; ?>
        <?php endif; ?>
        
        
    })
    
</script>

<div class="frameForm" align="left">
<form id="despesa" action="<?php echo url_for('despesa/'.($form->getObject()->isNew() ? 'createFinanciero' : 'updateFinanciero').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getCodigoSaida() : '').($id_projeto ? '&id_projeto='.$id_projeto : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <input type="hidden" name="referer" id="referer" value="<?php echo $ref ?>" />
    <table width="100%">  
        <tfoot>
            <tr>
              <td>
                    <?php echo $form->renderHiddenFields(false) ?>
                    <table cellspacing="4">
                      <tr>
                          <td>
                              <div class="button">
                                  <?php if($id_projeto): ?>
                                     <?php echo link_to(__('Voltar à lista'), '@default?module=despesa&action=index&id_projeto='.$id_projeto, array('class' => 'button')) ?>
                                  <?php else: ?>
                                      <?php echo link_to(__('Voltar à lista'), '@default?module=despesa&action=index', array('class' => 'button')) ?>
                                  <?php endif; ?>
                                  
                              </div>
                          </td>            
                          <?php if (!$form->getObject()->isNew()): ?>
                          <?php if($form->getObject()->getCentro() != 'compensação'): ?>
                          <td>
                              <div class="button">
                                  <?php echo link_to(__('Eliminar'), 'despesa/delete?id='.$form->getObject()->getCodigoSaida().'&despro=1', array('method' => 'delete', 'confirm' => __('Tem certeza de que quer apagar os dados selecionados?'), 'class' => 'button')) ?>
                              </div>
                          </td>
                          <?php endif; ?>
                          <?php endif; ?>
                          <td>
                            <input type="submit" value="<?php echo __('Salvar') ?>" />
                          </td>
                      </tr>
                  </table>
              </td>
            </tr>
        </tfoot>
        <tr>
            <td>
                &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são requeridos')?>
            </td>
        </tr>
        <tr>
            <td id="errorGlobal" colspan="2">
                <?php echo $form->renderGlobalErrors() ?>
            </td>
        </tr>
        <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%" id="table-info">
                    <tr>
                        <td style="width: 12%;"><?php echo $form['documentos']->renderLabel() ?></td>
                        <td style="width: 19%;">
                          <?php echo $form['documentos'] ?>
                          <?php echo $form['documentos']->renderError() ?>
                        </td>
                        <td style="width: 115px;"><?php echo $form['operacao']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['operacao'] ?>
                          <?php echo $form['operacao']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['centro']->renderLabel() ?></td>
                        <td colspan="3">
                          <?php echo $form['centro'] ?>
                          <?php echo $form['centro']->renderError() ?>
                        </td>
                    </tr>
                    
                    <tr id="projeto-tr">
                        <td><?php echo $form['codigoprojeto']->renderLabel() ?></td>
                        <td colspan="3">
                          <?php echo $form['codigoprojeto'] ?>
                          <?php echo $form['codigoprojeto']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['codigo_tipo']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['codigo_tipo'] ?>
                          <?php echo $form['codigo_tipo']->renderError() ?>
                        </td>
                        <td><?php echo $form['codigo_subtipo']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['codigo_subtipo'] ?>
                          <?php echo $form['codigo_subtipo']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><label><div id="lb-cadastro">Fornecedor</div></label></td>
                        <td colspan="3">
                          <?php echo $form['codigocadastro'] ?>
                          <?php echo $form['codigocadastro']->renderError() ?>
                        </td>
                    </tr>
                    <tr id="funcionario-tr">
                        <td><?php echo $form['codigofuncionario']->renderLabel() ?></td>
                        <td colspan="3">
                          <?php echo $form['codigofuncionario'] ?>
                          <?php echo $form['codigofuncionario']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['dataprevista']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['dataprevista'] ?>
                          <?php echo $form['dataprevista']->renderError() ?>
                        </td>
                        <td id="l-dataemissao"><?php echo $form['dataemissao']->renderLabel() ?></td>
                        <td id="c-dataemissao">
                          <?php echo $form['dataemissao'] ?>
                          <?php echo $form['dataemissao']->renderError() ?>
                        </td>
                    </tr>
                    <tr id="fecha-faturamento">
                        <td><?php echo $form['datarecebimentopre']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['datarecebimentopre'] ?>
                          <?php echo $form['dataprevista']->renderError() ?>
                        </td>
                        <td><?php echo $form['datareal']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['datareal'] ?>
                          <?php echo $form['datareal']->renderError() ?>
                        </td>
                        
                    </tr>
                    
                    
                    
                    <tr>
                        <td><?php echo $form['saidaprevista']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['saidaprevista'] ?>
                          <?php echo $form['saidaprevista']->renderError() ?>
                        </td>
                        <td><?php echo $form['saidas']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['saidas'] ?>
                          <?php echo $form['saidas']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['descricaosaida']->renderLabel() ?></td>
                        <td colspan="3">
                          <?php echo $form['descricaosaida'] ?>
                          <?php echo $form['descricaosaida']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['formapagamento']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['formapagamento'] ?>
                          <?php echo $form['formapagamento']->renderError() ?>
                        </td>
                        <td><?php echo $form['tipo']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['tipo'] ?>
                          <?php echo $form['tipo']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <?php if(!aplication_system::esUsuarioRoot()): ?>
                        <td><?php echo $form['baixa']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['baixa'] ?>
                          <?php echo $form['baixa']->renderError() ?>
                        </td>
                        <?php endif; ?>
                        <?php if(aplication_system::esUsuarioRoot()): ?>
                        <td><?php echo $form['confirmacao']->renderLabel() ?></td>
                        <td>
                          <?php echo $form['confirmacao'] ?>
                          <?php echo $form['confirmacao']->renderError() ?>
                        </td>
                        <?php endif; ?>
                    </tr>
                    
                    
                    
                </table>
            </td>
        </tr>
    </table>
    
    


</form>
</div>